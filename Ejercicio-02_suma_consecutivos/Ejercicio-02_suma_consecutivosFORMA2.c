#include <stdio.h>
#include <math.h> 
int main()
{
    int num;
    int contador;
    int suma = 0;
    printf("Ingrese un numero entero entre 1 y 50 : ");
    scanf ("%d", &num);

    for(contador = 1; contador <= num; contador++){
        suma = suma + contador;
    }//fin for
    
    printf("La sumatoria de numeros consecutivos es: ");
    printf("%d\n", suma);
    return 0;
}