#include <stdio.h>
#include <math.h> 
int main()
{
    int num;
    int suma = 0;
    printf("Ingrese un numero entero entre 1 y 50 : ");
    scanf ("%d", &num);
    suma = num * (num + 1)/2;
    printf("La sumatoria de numeros consecutivos es: ");
    printf("%d\n", suma);
    return 0;
}