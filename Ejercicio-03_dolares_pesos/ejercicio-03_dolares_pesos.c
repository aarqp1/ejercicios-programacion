#include <stdio.h>

int main()
{
    double cantDolar;
    double valorDolar;
    double pesos;
    
    printf("Ingrese un monto en dolares: ");
    scanf("%lf", &cantDolar);
    printf("Ingrese el valor actual del peso mexicano frente al dolar: ");    
    scanf("%lf", &valorDolar);
    
    pesos = valorDolar * cantDolar;
    printf("El valor de dolares : %.2f", cantDolar);
    printf(" en pesos mexicanos es: %.2f", pesos);
    
    return 0;
}
