#include <stdio.h>

int main()
{
    double medida;
    double perimetro;
    
    printf("Ingrese la medida de un lado del triangulo: ");
    scanf("%lf",&medida);
    perimetro = medida * 3;
    printf("El perimetro del triangulo es: %.2lf", perimetro);
    
    return 0;
}
