#include <stdio.h>

int main()
{
    double base;
    double lado;
    double perimetro;
    
    printf("Ingrese el valor de un lado: ");
    scanf("%lf",&lado);
    printf("Ingrese el valor de la base: ");
    scanf("%lf",&base);
    
    perimetro = base + (lado * 2);
    
    printf("El perimetro es : %.2lf", perimetro);
    return 0;
}
