#include <stdio.h>

int main()
{
    double base;
    double lateral;
    double opuesto;
    double perimetro;
    
    printf("Ingrese el valor de la base del tríangulo: ");
    scanf("%lf",&base);
    printf("Ingrese el valor del lado opuesto del triangulo: ");
    scanf("%lf",&opuesto);
    printf("Ingrese el valor del lado lateral del triangulo: ");
    scanf("%lf",&lateral);
    printf("Ingrese el valor de la base: ");

    perimetro = base + lateral + opuesto;
    
    printf("El perimetro es : %.2lf", perimetro);
    return 0;
}